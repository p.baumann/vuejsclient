const VueLoaderPlugin = require('vue-loader/lib/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const stylus_plugin = require('stylus_plugin');

module.exports = {
    module: {
        rules: [
            {
                test: /\.vue$/u,
                exclude: /node_modules/u,
                loader: 'vue-loader'
            },
            {
                test: /\.sass$/u,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            indentedSyntax: true
                        }
                    }
                ]
            },
            {
                test: /\.styl$/u,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'stylus-loader',
                        options: {
                            use: [stylus_plugin()],
                        },
                    },
                ],
            },
            {
                test: /\.js$/u,
                exclude: /node_modules/u,
                use: {loader: 'babel-loader', options: project.build.babel}
            }

        ]

    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            'Components': path.resolve(__dirname, 'src/components')
        }
    },
    plugins: [
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new Webpack.DefinePlugin({
            VUE_APP_API_URL: JSON.stringify('https://0.0.0.0:8443')
        })
    ]
};