import Vue from 'vue'
import Vuex from 'vuex'
import VueCookies from 'vue-cookies'
import axios from 'axios'
import jwtdecode from 'jwt-decode'


Vue.use(Vuex);
Vue.use(VueCookies);

var user = localStorage.getItem('user');
var decoded = localStorage.getItem('decoded');
try {
    user = JSON.parse(user);
    decoded = JSON.parse(decoded);
} catch (e) {
    console.log('Could not JSONparse from localstorage');
}

let state = {
    status: 'success',
    loading: false,
    token: localStorage.getItem('token') || null,
    decoded: decoded,
    user: user,
    drawer: true,
    loggedIn: false,
};

export default new Vuex.Store({
    state,
    mutations: {
        reset_state() {
            return {
                token: null,
                decoded: {},
                user: null,
                status: 'loggedOut',
                loading: false,
                loggedIn: false,
                drawer: true,
            }
        },
        auth_request(state) {
            state.status = 'loading';
            state.loading = true;
        },
        request_pending(state) {
            state.status = 'loading';
            state.loading = true;
        },
        request_success(state) {
            state.status = 'success';
            state.loading = false;
        },
        request_error(state) {
            state.status = 'error';
            state.loading = false;
        },
        auth_success(state, data) {
            state.status = 'success';
            state.loading = false;
            state.token = data.res.data.token;
            state.user = data.user;
            state.decoded = data.decoded;
            state.decoded.exp_date = new Date(data.decoded.exp * 1000);
            state.decoded.exp_ms = (state.decoded.exp - state.decoded.iat) * 1000;
            localStorage.setItem('token', data.res.data.token);
            localStorage.setItem('decoded', JSON.stringify(data.decoded));
            console.log(data.user)
            localStorage.setItem('user', JSON.stringify(data.user));
        },
        auth_error(state) {
            state.status = 'error';
            state.loading = false;
        },
        logout(state) {
            localStorage.clear();
            state.token = null;
            state.decoded = {};
            state.user = null;
            state.loggedIn = false;
            state.status = 'loggedOut';
            state.loading = false;
        },
        toggle_drawer(state) {
            if (state.loggedIn) state.drawer = !state.drawer;
        },
        setUser(state) {
            localStorage.setItem('user', state.user);
        },
    },
    actions:
        {
            login({commit}, {router, user}) {
                Vue.toasted.clear();
                let pendingToast = Vue.toasted.global.requestPending();
                return new Promise((resolve, reject) => {
                    commit('auth_request');
                    Vue.prototype.$http.post('/login-check', {username: user.email, password: user.password})
                        .then(res => {
                            let token = res.data.token
                            let decoded = null;
                            try {
                                decoded = jwtdecode(token);
                            } catch (err) {
                                throw new Error('Failed to parse the token')
                            }
                            axios.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.token;
                            commit('auth_success', {user, res, decoded});
                            commit('setUser');
                            pendingToast.remove();
                            Vue.toasted.global.requestSuccess();
                            router.push('/');
                            resolve(res)
                        })
                        .catch(err => {
                            commit('auth_error', err);
                            pendingToast.remove();
                            Vue.toasted.global.requestFailed({message: err});
                            reject(err)
                        })
                })
            },
            register({commit}, user) {
                return new Promise((resolve, reject) => {
                    commit('auth_request');
                    Vue.prototype.$http.post('/register', {email: user.email, password: user.password})
                        .then(res => {
                            resolve(res)
                        })
                        .catch(err => {
                            commit('auth_error');
                            reject(err)
                        })
                })
            },
            logout({commit}) {
                commit('logout')
                commit('toggle_drawer')
                commit('reset_state')
            },
            drawer({commit}) {
                commit('toggle_drawer');
            },
            fetchUsers({commit}) {
                Vue.toasted.clear();
                let pendingToast = Vue.toasted.global.requestPending();
                return new Promise((resolve, reject) => {
                    commit('request_pending');
                    Vue.prototype.$http.get('/api/users/').then(res => {
                        let data = res.data
                        commit('request_success');
                        pendingToast.remove();
                        Vue.toasted.global.requestSuccess();
                        resolve(data)
                    })
                        .catch(err => {
                            commit('request_error', err);
                            pendingToast.remove();
                            Vue.toasted.global.requestFailed({message: err});
                            reject(err)
                        })
                })
            },
            callApi({commit}) {
                let pendingToast = Vue.toasted.global.requestPending();
                return new Promise((resolve, reject) => {
                    Vue.prototype.$http.get('/api/users/').then(res => {
                        let data = res.data
                        pendingToast.remove();
                        Vue.toasted.global.requestSuccess({message: res.data});
                        resolve(data)
                    })
                        .catch( err => {
                            pendingToast.remove();
                            Vue.toasted.global.requestFailed({message: err});
                            reject(err)
                        })
                })
            }
        },
    getters: {
        getAppStatus: state => state.status
    },
    methods: {
        loggedIn: function (state) {
            let user = localStorage.getItem('user');
            return user ? user : null;
        }
    },
})
