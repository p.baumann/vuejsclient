export const exampleFormParams = {
    model: {
        id: 1,
        name: "John Doe",
        password: "J0hnD03!x4",
        skills: ["Javascript",
"VueJS"],
        email: "john.doe@gmail.com",
        status: true,
        number: 1,
        time: null,
        rating: 1,
        usePassword: true,
        date: new Date().toISOString()
.substr(0, 10)
    },
    schema: {
        fields: [
            {
                type: "DatePicker",
                label: "DatepickerCustom",
                model: "date",
                readonly: false,
                placeholder: new Date().toISOString()
.substr(0, 10),
                default: new Date().toISOString()
.substr(0, 10),
            },
            {
                type: "customCheckbox",
                label: "Agreement",
                model: "status"
            },
            {
                type: "customNumber",
                label: "Status",
                model: "number",
                default: 1,
                max: 9000,
                icon: {
                    append: null,
                    prepend: null
                }
            },
            {
                type: "timePicker",
                label: "At",
                model: "time",
                readonly: false,
            },
            {
                type: "rating",
                label: "Rating based on last surveys :",
                model: "rating",
            },
            {
                type: "customSlider",
                label: "Toxicity level",
                model: "number",
                max: 100,
                step: 10,
                readonly: false,
            }
        ],
    },
    options: {},
    computed: {
        rules() {
            const rules = []

            if (this.max) {
                const rule =
                    v => (v || '').length <= this.max ||
                        `A maximum of ${this.max} characters is allowed`

                rules.push(rule)
            }

            if (!this.allowSpaces) {
                const rule =
                    v => (v || '').indexOf(' ') < 0 ||
                        'No spaces are allowed'

                rules.push(rule)
            }

            if (this.match) {
                const rule =
                    v => (!!v && v) === this.match ||
                        'Values do not match'

                rules.push(rule)
            }

            return rules
        }
    },
};
