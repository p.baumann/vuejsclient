import axios from "axios";

export function isNameJoe(value) {
    if (!value) return true;
    return value === "Joe";
}

export function notGmail(value = "") {
    return !value.includes("@");
}

export function isEmailAlreadyRegistered(value = ""){
    if (value === "" || null) return true;
    return axios.post('/check-email',{email:value})
        .then(() => {
            return true;
        })
        .catch(() => {
            return false;
    });
}
