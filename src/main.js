import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Axios from 'axios'
import Vuelidate from "vuelidate"
import BootstrapVue from 'bootstrap-vue'
import Toasted from 'vue-toasted'
import VueMaterial from 'vue-material'
import VueFormGenerator from "vue-form-generator"
import Vuetify from 'vuetify'
import VueCookies from 'vue-cookies'

//import '../public/material-dashboard.css';
import '../public/app.css'
import 'vuetify/dist/vuetify.min.css'

const mixinExample = {
    created: () => {
        console.warn('Mixin hook example triggered')
    }
};

import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
    theme: {
        primary: '#4776bc',
        secondary: colors.indigo.base,
        accent: colors.indigo.base
    },

});

Vue.config.productionTip = false;

Vue.use(VueCookies);
Vue.use(BootstrapVue);
Vue.use(VueMaterial);

import DatePicker from "./components/form/inputs/fieldDatePicker";
import TimePicker from "./components/form/inputs/fieldTimePicker";
import CustomNumber from "./components/form/inputs/fieldCustomNumber.vue";
import RatingField from "./components/form/inputs/fieldRating.vue";
import CustomCheckboxField from "./components/form/inputs/fieldCustomCheckbox.vue";
import CustomSlider from "./components/form/inputs/fieldCustomSlider.vue";

Vue.component("fieldCustomNumber", CustomNumber);
Vue.component("fieldDatePicker", DatePicker);
Vue.component("fieldTimePicker", TimePicker);
Vue.component("fieldRating", RatingField);
Vue.component("fieldCustomCheckbox", CustomCheckboxField);
Vue.component("fieldCustomSlider", CustomSlider);
Vue.use(VueFormGenerator);
Vue.use(Vuelidate);
Vue.use(Toasted, {iconPack: 'material-icons', router});

Vue.prototype.$http = Axios;

Axios.defaults.baseURL = process.env.VUE_APP_API_URL;

Axios.interceptors.request.use(config => {
    NProgress.start();
    return config
}, error => {
    NProgress.done();
    return Promise.reject(error);
});

Axios.interceptors.response.use(response => {
    NProgress.done();
    return response
}, error => {
    NProgress.done();
    store.state.status = 'error';
    return Promise.reject(error);
});

Vue.config.productionTip = false;

let failedOptions = {
    type: 'error',
    icon: 'error_outline',
    duration: 5000
};
let successOptions = {
    type: "success",
    icon: 'check',
    duration: 1000
};

Vue.toasted.register('requestPending', 'Connecting to server...', {
    type: "info",
    icon: 'cached',
    duration: 15000
});

Vue.toasted.register('requestSuccess', payload => {
    if (!payload.message) return 'Done';
    return `Success : ${payload.message}`;
}, successOptions);

Vue.toasted.register('requestFailed', err => {
    if (!err.message) return 'Something went wrong...';
    return err.message;
}, failedOptions);

Vue.config.errorHandler = function (err, vm, info) {
    let current = vm,
        handler
    if (vm.$options.errorHandler) {
        handler = vm.$options.errorHandler
    } else {
        while (current.$parent) {
            current = current.$parent;
            if (handler === current.$options.errorHandler) break
        }
    }
    if (handler) handler.call(current, err, vm, info)
};

Vue.filter('two_digits', function (value) {
    if (value.toString().length <= 1) {
        return "0" + value.toString();
    }
    return value.toString();
});

new Vue({
    el: '#app',
    validation: {},
    mixins: [mixinExample],
    router,
    store,
    render: h => h(App)
});
