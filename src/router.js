import Vue from 'vue'
import Router from 'vue-router'
import store from './store.js'
import Login from './views/Login.vue'
import Dashboard from "./views/Dashboard";
import Register from "./views/Register";
import VuetifyPage from "./views/BasePageVuetify";
import UI from "./views/UI";

Vue.use(Router);

let router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/dashboard',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                requiresAuth: true,
                breadcrumb: 'dashboard'
            },
            icon: 'dashboard',
            navigation: true
        },
        {
            path: '/home',
            name: 'home',
            component: Dashboard,
            meta: {
                requiresAuth: true,
                breadcrumb: 'home'
            },
            icon: 'fa-home',
            navigation: true
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            icon: 'fa-lock-open',
            navigation: true,
            meta: {
                breadcrumb: 'login'
            }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            icon: 'fa-user-plus',
            navigation: true,
            meta: {
                breadcrumb: 'register'
            }
        },
        {
            path: '/ui',
            name: 'UI',
            component: UI,
            icon: 'fa-csdcsdfsdf',
            navigation: true,
            meta: {
                breadcrumb: 'UI'
            }
        },
        {
            path: '/vuetify2',
            name: 'vuetify2',
            open:['vuetify2'],
            component: VuetifyPage,
            icon: 'fa-file-alt',
            navigation: true,
            meta: {
                breadcrumb: 'vuetify2'
            },
            children: [
                {
                    name: 'lvl1',
                    path: 'lvl1',
                    component: VuetifyPage,
                    meta: {
                        breadcrumb: 'lvl1'
                    },
                    children: [
                        {
                            name: 'lvl2',
                            path: 'lvl2',
                            component: VuetifyPage,
                            meta: {
                                breadcrumb: 'lvl2'
                            },
                            children: [
                                {
                                    name: 'lvl3',
                                    path: 'lvl3',
                                    component: VuetifyPage,
                                    meta: {
                                        breadcrumb: 'lvl3'
                                    },
                                    children: [
                                        {
                                            name: 'lvl4',
                                            path: 'lvl4',
                                            component: VuetifyPage,
                                            meta: {
                                                breadcrumb: 'lvl4'
                                            },
                                        },
                                    ]
                                },
                                {
                                    name: 'lvl3.2',
                                    path: 'lvl3.2',
                                    component: VuetifyPage,
                                    meta: {
                                        breadcrumb: 'lvl3.2'
                                    },
                                    children: [
                                        {
                                            name: 'lvl4.2',
                                            path: 'lvl4.2',
                                            component: VuetifyPage,
                                            meta: {
                                                breadcrumb: 'lvl4.2'
                                            },
                                            children: [
                                                {
                                                    name: 'lvl5',
                                                    path: 'lvl5',
                                                    component: VuetifyPage,
                                                    meta: {
                                                        breadcrumb: 'lvl5'
                                                    },
                                                    children: [
                                                        {
                                                            name: 'lvl6',
                                                            path: 'lvl6',
                                                            component: VuetifyPage,
                                                            meta: {
                                                                breadcrumb: 'lvl6'
                                                            },
                                                        },
                                                    ]
                                                },
                                            ]
                                        },
                                    ]
                                },
                            ]
                        },
                    ]
                },
            ]
        },
        {
            path: '*',
            redirect: '/dashboard',
            icon: 'fa-file-alt',
            navigation: false
        },
    ]
});

router.beforeEach((to, from, next) => {
    store.state.status = 'loading';
    store.state.loading = true;
    NProgress.start()

    if (to && to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.state.decoded.exp_date || store.state.decoded.exp_date <= new Date() || !store.state.loggedIn) {
            store.state.status = 'loggedout';
            store.state.loggedIn = false;
            router.push('/login')
        }
    }

    setTimeout(() => {
        // Extend router for an additional 200ms
        store.state.status = 'success';
        next()
    }, 500)
});

router.afterEach(() => {
    store.state.status = '';
    store.state.loading = false;
    NProgress.done();
});

export default router
