module.exports = {
    devServer: {
        port: 8080,
        host: 'expanded.space',
        open: true,
        disableHostCheck: true
    }
};
